"use strict";

const Form = (props) => {
  console.log("GUYSrerender....", props.shortcut);
  const [shortcut, setShortcut] = React.useState(props.shortcut);
  const [text, setText] = React.useState(props.text);
  const [color, setColor] = React.useState(props.color);
  const [newNotePreview, setNewNotePreview] = React.useState({
    out: false,
    color: "#fa7e0e",
    text: "",
    shortcut: "",
  });

  React.useEffect( () => {
    console.log("Butnow form should rerender....", props.shortcut);
    setShortcut(props.shortcut);
    setText(props.text);
    setColor(props.color);

    setNewNotePreview({
      out: false,
      color: props.color,
      text: "",
      shortcut: props.shortcut,
    });
  }, [props.shortcut, props.text, props.color]);
  

  const colorsAvailable = [
    "#fa7e0e",
    "#278741",
    "#a13f1b",
    "#1a729e",
    "#7b228f",
    "#4a4646",
    "#8e9915",
    "#940f0f",
  ];
  // not only one value...

  const handleShortcutChange = (e) => {
    const newShortcut = e.target.value;
    setShortcut(newShortcut);
    setNewNotePreview({ ...newNotePreview, shortcut: newShortcut });
  };

  const handleTextChange = (e) => {
    setText(e.target.value);
  };

  const handleColorChange = (e) => {
    const newColor = e.target.getAttribute("data-color");
    setColor(newColor);
    setNewNotePreview({ ...newNotePreview, color: newColor });
  };

  const colorButtons = colorsAvailable.map((color, i) => {
    const style = { background: color };
    return (
      <button
        key={i}
        onClick={handleColorChange}
        type="button"
        style={style}
        data-color={color}
      ></button>
    );
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if(shortcut.length === 0) {
      return false;
    }

    // reset form if new
    if (props.noteId === 0) {
      setShortcut("");
      setText("");
      setColor("#fa7e0e");
      setNewNotePreview({
        out: false,
        color: "#fa7e0e",
        text: "",
        shortcut: "",
      });
    }

    props.formSubmitHandler(props.noteId, text, shortcut, color, e);
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="shortcut-and-colors">
        <input
          placeholder="Tytuł/skrót (min. 1 znak)"
          className="input-shortcut"
          type="text"
          value={shortcut}
          onChange={handleShortcutChange}
        />
        {colorButtons}
      </div>
      <label className="input-text-label">
        <textarea
          placeholder="Np. spotkanie lub deadline"
          className="input-text"
          value={text}
          onChange={handleTextChange}
        />
      </label>
      {/* also a PREVIEW widnow */}
      <div className="text-right">
        {/* <span class="preview-text">Podgląd:</span> */}
        <NoteButtons notes={[newNotePreview]}></NoteButtons>
        <input className="save-button" type="submit" value="Zapisz" />
      </div>
    </form>
  );
};
