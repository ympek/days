const CalendarGrid = (props) => {
  const months = [0,1,2,3,4,5,6,7,8,9,10,11].map((monthIndex) => {
    const isCurrent = props.today.getMonth() === monthIndex; 
    
    return (
      <Month key={monthIndex} // couldn't they be together??
           idx={monthIndex}
           isCurrent={isCurrent} 
           year={2020} 
           squareClickCallback={props.squareClickCallback} 
           notes={props.notes}
           selectedDay={props.selectedDay} />
    );
  });

  return <div>{months}</div>
}