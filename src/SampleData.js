const sampleNotes = {
  "2020-10-06": [
    {
      id: 1,
      out: false,
      color: "#fa7e0e",
      shortcut: "Typowe lorem ipsum",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release ",
    },
    { 
      id: 2, 
      out: true, 
      color: "#1a729e", 
      shortcut: "Call mom", 
      text: "Najlepiej po 18:00 bo będzie w kościele" 
    },
  ],

  "2020-10-08": [
    {
      id: 6,
      out: false,
      color: "#278741",
      shortcut: "Zakupy",
      text: "mleko, chleb, cos jeszcze(?)",
    },
  ],

  "2020-10-09": [
    {
      id: 3,
      out: false,
      color: "#940f0f",
      shortcut: "Dominik",
      text: "16:00, call z Dominikiem; uwaga: przygotować demo wcześniej",
    },
    { id: 4, 
      out: true, 
      color: "#8e9915", 
      shortcut: "Wyjazd", 
      text: "jedziemy Gda->Osw" 
    },
  ],
  "2020-10-14": [
    {
      id: 5,
      out: false,
      color: "#a13f1b",
      shortcut: "Samotna notatka",
      text: "Ta notatka nie ma koleżanek.",
    },
  ],
  "2020-10-24": [
    {
      id: 7,
      out: false,
      color: "#4a4646",
      shortcut: "Piwko",
      text: "Ale nie wiem jeszcze gdzie i z kim.",
    },
  ],
  "2020-10-25": [
    {
      id: 8,
      out: false,
      color: "#7b228f",
      shortcut: "odpocząć",
      text: "",
    },
  ],
};
