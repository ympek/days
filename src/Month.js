const Month = (props) => {
  const monthDiv = React.useRef(null);
  
  React.useEffect(() => {
    if (props.isCurrent) {
      monthDiv.current.scrollIntoView();
    }
  }, [props.isCurrent]);

  const createDummySquares = () => {
    const firstOfTheMonth = new Date(props.year, props.idx, 1);
    const dayIdx = firstOfTheMonth.getDay();
    const numOfDummySquares = dayIdx === 0 ? 6 : dayIdx - 1; // 0 is sunday  
    let dummySquares = [];
    for (let i = 0; i < numOfDummySquares; i++) {
      dummySquares.push(<DummySquare key={i} />);
    }
    return dummySquares;
  };

  const createOperationalSquares = () => {
    return generateDatesOfAMonth(props.idx).map(date => {
      const dateKey = date.keyify();
      const notesToPass = props.notes[dateKey] ? props.notes[dateKey] : [];
      
      return <DaySquare selectedDay={props.selectedDay} key={dateKey} date={date} squareClickCallback={props.squareClickCallback} notes={notesToPass} />
    });
  }
  
  const dummySquares = createDummySquares();
  const squares = createOperationalSquares();
  
  return (
    <div ref={monthDiv}>
      <a name={"month-" + props.idx} />
      <h1>{ monthToPolishMonthName(props.idx) + " " + props.year }</h1>
      <div className="calendar-grid">
        { dummySquares }
        { squares }
      </div>
    </div>
  )
};