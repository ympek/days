'use strict';

class FloatingWindow extends React.Component {
  constructor(props) {
    super(props);

    let pageWidth = document.body.clientWidth;

    this.squareWidthInPixels = 257; // should be calculated.

    this.state = {
      width: (0.25 * pageWidth)
    };
  }

  recalculatePosition() {
    const ownWidth = this.state.width;
    const viewportWidth = document.body.clientWidth;
    const top = this.props.posTop;
    const left = this.props.posLeft; // target square pos left actually

    if (ownWidth + left + this.squareWidthInPixels > viewportWidth) // wystaje
    {
      return { 
        top: top + window.scrollY, 
        left: left - ownWidth,
        width: ownWidth + "px"
      }
    }
    else {
      return { 
        top: top + window.scrollY, 
        left: left + this.squareWidthInPixels,
        width: ownWidth + "px"
      }
    }
  }
    
  render() {
    const cssClasses = "floating-window " + (this.props.visible ? "visible" : "");
    const dynamicStyle = this.recalculatePosition();
    return (
      <div className={cssClasses} style={dynamicStyle} onClick={(e) => { e.stopPropagation(); }}>
        {this.props.children}
      </div>
    );
  }
}