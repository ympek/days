'use strict';

class MonthSelector extends React.Component {
  constructor(props) {
    super(props);
  }
  
  createMonthShortcuts() {
    let shortcuts = [];
    for (let i = 0; i < 12; i++) {
      shortcuts.push(monthToPolishMonthName(i).substring(0, 3));
    }
    return shortcuts;
  }

  render() {
    const monthShortcuts = this.createMonthShortcuts().map( (shortcut, idx) =>
      <a key={idx} className="month-link" href={ "#month-" + idx }>{shortcut}</a>
    );

    return (
      <div className="month-selector">
        { monthShortcuts }
      </div>
    );
  }
}