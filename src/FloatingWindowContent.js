const Notes = (props) => {
  const handleEditButtonClick = (entry, e) => {
    props.editNoteHandler(entry);
  };

  const handleDeleteButtonClick = (entry, e) => {
    props.deleteNoteHandler(entry.id);
  };

  const notes = props.notes;
  console.log("NOTES", notes);

  if (notes.length === 0) {
    return <div className="nothing-here">Nic tu nie ma!</div>;
  }

  return notes.map((entry) => {
    const styles = { background: entry.color, padding: "5px" };
    return (
      <div key={entry.id}>
        <div className="note-indicator" style={styles}>
          {entry.shortcut}
        </div>
        <a
          onClick={(e) => handleEditButtonClick(entry, e)}
          className="a-text-edit"
        >
          edytuj
        </a>

        <a
          onClick={(e) => handleDeleteButtonClick(entry, e)}
          className="a-text-del"
        >
          usuń
        </a>

        <div className="note-content">{entry.text}</div>
      </div>
    );
  });
};

const FloatingWindowContent = (props) => {
  if (!props.date) {
    return <div>Brak informacji.</div>;
  }

  const [selectedNoteId, setSelectedNoteId] = React.useState(0);
  const [selectedNoteColor, setSelectedNoteColor] = React.useState("#fa7e0e");
  const [selectedNoteShortcut, setSelectedNoteShortcut] = React.useState("");
  const [selectedNoteText, setSelectedNoteText] = React.useState("");

  // TODO replace all occurences of #fa7e0e to some constant DEFAULT_COLOR Or....
  // const [currentNote, setCurrentNote] = React.useState({ out: false, color: "#fa7e0e", })

  const day = new Date(props.date);
  const dateKey = day.keyify();
  const ownNotes = props.notes[dateKey] ? props.notes[dateKey] : []; // hmm TODO

  const handleEditNote = (noteObj, e) => {
    console.log("Handling edit...", noteObj);
    setSelectedNoteId(noteObj.id);
    setSelectedNoteColor(noteObj.color);
    setSelectedNoteShortcut(noteObj.shortcut);
    setSelectedNoteText(noteObj.text);
  };

  const handleNewNote = () => {
    setSelectedNoteId(0);
    setSelectedNoteColor("#fa7e0e");
    setSelectedNoteShortcut('');
    setSelectedNoteText('');
  }

  React.useEffect(() => {
    console.log("Effect -> handleNew Note");
    handleNewNote();
  }, [props.date]);

  // separate in from out notes
  // let inNotes = [], outNotes = [];
  // if (ownNotes.length > 0) {
  //   ownNotes.forEach( (note) => note.out ? outNotes.push(note) : inNotes.push(note) );
  // }

  return (
    <div>
      <h2>
        <SquareHeaderDate date={day} />
      </h2>
      <Notes
        notes={ownNotes}
        editNoteHandler={handleEditNote}
        deleteNoteHandler={props.deleteNoteHandler}
      />

      <div className="form-cnt">
        <p className="new-note">
          <a onClick={handleNewNote} className="new-note-link">Nowa notatka?</a>
        </p>
        <Form
          noteId={selectedNoteId}
          shortcut={selectedNoteShortcut}
          text={selectedNoteText}
          color={selectedNoteColor}
          formSubmitHandler={props.formSubmitHandler}
        />
      </div>
    </div>
  );
};
