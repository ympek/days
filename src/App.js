"use strict";

window.location.hash = '';

const FormattedDate = (props) => {
  const dateString =
    props.date.toLocaleDateString() +
    " " +
    dayToPolishDayName(props.date.getDay());
  return <span>{dateString}</span>;
};

const SquareHeaderDate = (props) => {
  const dateString =
    dayToPolishDayName(props.date.getDay()) +
    ", " +
    props.date.getDate() +
    " " +
    monthToPolishMonthName(props.date.getMonth()).substring(0, 3) +
    " " +
    props.date.getFullYear();

  return <span>{dateString}</span>;
};

(function initNextId(initialNotesObj) {
  for (const day in initialNotesObj) {
    if (initialNotesObj.hasOwnProperty(day)) {
      nextId += initialNotesObj[day].length;
    }
  }
})(sampleNotes);

// IDEA
// You can add "in-notes" only for THIS and FOLLOWING days
// You can add "out-notes" only for THIS and PREVIOUS days.

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      today: new Date(), // state czy props : P
      selectedDay: "",
      selectedFullDate: "",
      notes: sampleNotes,
      floatingWindowVisible: false,
      floatingWindowContents: "",
      floatingWindowPositionTop: 0,
      floatingWindowPositionLeft: 0,
      notesToPass: [],
    };

    this.handleSquareClick = this.handleSquareClick.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.closeFloatingWindow = this.closeFloatingWindow.bind(this);
    this.deleteNoteById = this.deleteNoteById.bind(this);
  }

  closeFloatingWindow(e) {
    this.setState({
      floatingWindowVisible: false,
      selectedDay: "",
      selectedFullDate: "", // will refactor this later.
    });
  }

  showLoader() {
    this.setState({
      floatingWindowContents: <Loader />,
    });
  }

  // do rozbudowania
  handleFormSubmit(id, text, shortcut, color, e) {
    e.preventDefault();

    if (id) {
      this.updateNoteById(id, text, shortcut, color);
      return;
    }

    if (this.state.notes[this.state.selectedDay]) {
      this.setState((state, props) => ({
        notes: {
          ...state.notes,
          [state.selectedDay]: [
            ...state.notes[state.selectedDay],
            {
              id: getNextAvailableId(),
              out: false,
              color: color,
              shortcut: shortcut,
              text: text,
            },
          ],
        },
      }));
    } else {
      this.setState((state, props) => ({
        notes: {
          ...state.notes,
          [state.selectedDay]: [
            {
              id: getNextAvailableId(),
              out: false,
              color: color,
              shortcut: shortcut,
              text: text,
            },
          ],
        },
      }));
    }
  }

  handleSquareClick(date, e) {
    e.stopPropagation();

    const dateKey = date.keyify();

    if (dateKey === this.state.selectedDay) {
      this.closeFloatingWindow();
      return;
    }

    const rect = e.target.getBoundingClientRect();

    this.setState((state) => ({
      selectedDay: dateKey,
      selectedFullDate: date,
    }));

    this.setState({
      floatingWindowVisible: true,
      floatingWindowContents: "", // deprecated
      floatingWindowPositionTop: rect.top,
      floatingWindowPositionLeft: rect.left,
    });
  }

  updateNoteById(id, text, shortcut, color) {
    let newNotesForDay = [];

    this.state.notes[this.state.selectedDay].forEach((note) => {
      if (note.id !== id) {
        newNotesForDay.push(note);
      } else {
        newNotesForDay.push({
          id: id,
          out: false,
          shortcut: shortcut,
          color: color,
          text: text,
        });
      }
    });

    this.setState((state, props) => ({
      notes: {
        ...state.notes,
        [state.selectedDay]: newNotesForDay,
      },
    }));
  }

  deleteNoteById(id) {
    // definitely can be done prettier.
    // alert("deleting Note By Id" + id);

    let newNotesForDay = [];

    this.state.notes[this.state.selectedDay].forEach((note) => {
      if (note.id !== id) {
        newNotesForDay.push(note);
      }
    });

    this.setState((state, props) => ({
      notes: {
        ...state.notes,
        [state.selectedDay]: newNotesForDay,
      },
    }));
  }

  render() {
    return (
      <div className="wrapper" onClick={this.closeFloatingWindow}>
        <nav className="side-pane" id="month-selector">
          <div className="img-wrapper">
            <img src="days_logo_light.png" />
          </div>
          <MonthSelector today={this.state.today} />
        </nav>

        <main className="main-pane">
          <CalendarGrid
            today={this.state.today}
            squareClickCallback={this.handleSquareClick}
            notes={this.state.notes}
            selectedDay={this.state.selectedDay}
          />
        </main>

        <FloatingWindow
          formSubmitHandler={this.handleFormSubmit}
          visible={this.state.floatingWindowVisible}
          posTop={this.state.floatingWindowPositionTop}
          posLeft={this.state.floatingWindowPositionLeft}
        >
          <FloatingWindowContent
            date={this.state.selectedFullDate}
            notes={this.state.notes}
            formSubmitHandler={this.handleFormSubmit}
            deleteNoteHandler={this.deleteNoteById}
          />
        </FloatingWindow>
      </div>
    );
  }
}

let domContainer = document.querySelector("#root");
ReactDOM.render(<App />, domContainer);
