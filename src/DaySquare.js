'use strict';

const DummySquare = (props) => {
  return <div className="date-box dummy"></div>
};

const DaySquare = (props) => {

  if(isToday(props.date)) {
    console.log("Let's debugo");
    console.log(props.date, props.selectedDay, props.date.keyify());
  }

  const cssClasses = "date-box operational" + (isToday(props.date) ? " today" : "") 
                                            + (props.selectedDay === props.date.keyify() ? " selected" : "");
  
  return (
    <div className={cssClasses} onClick={(e) => props.squareClickCallback(props.date, e)}>
      <FormattedDate date={props.date} />
      <br/>
      <br/>
      <NoteButtons notes={props.notes} />
    </div>
  );
};

const NoteButtons = (props) => {
  return (
    props.notes.map((note, index) => {
      const style = { background: note.color };
      return <div key={index} className="note-indicator" style={style}>{note.shortcut}</div>
    })
  )
};