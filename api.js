"use strict";

async function getAllDates() {
  const res = await fetch('/all');
  const data = await res.json();
  console.log("fetched all!");
  console.log(data);
  return data;
};