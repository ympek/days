function dayToPolishDayName(day) {
  switch (day) {
    case 0: return "niedziela"
    case 1: return "poniedziałek"
    case 2: return "wtorek"
    case 3: return "środa"
    case 4: return "czwartek"
    case 5: return "piątek"
    case 6: return "sobota"
    default: return "invalid_day"
  }
}

function monthToPolishMonthName(month) {
  month = month % 12;
  switch (month) {
    case 0: return "styczeń"
    case 1: return "luty"
    case 2: return "marzec"
    case 3: return "kwiecień"
    case 4: return "maj"
    case 5: return "czerwiec"
    case 6: return "lipiec"
    case 7: return "sierpień"
    case 8: return "wrzesień"
    case 9: return "październik"
    case 10: return "listopad"
    case 11: return "grudzień"
    default: return "invalid_month"
  }
}

function isToday(date) {
  const today = new Date();
  return date.getDate()  === today.getDate()
      && date.getMonth() === today.getMonth()
      && date.getYear()  === today.getYear()
}

/**
 * @param {number} month - integer between 0 and 11, representing the month. 0 corresponds to January.
 */
function generateDatesOfAMonth(month, year = 2020) { // TODO : need to think what to do with this year thing.
  let dates = [];
  let currDate = new Date(year, month, 1, 2);
  let firstOfNextMonth = new Date(year, month + 1, 2); // align UTC with UTC+2 - minor bug I didnt catch earlier

  do {
    dates.push(new Date(currDate.valueOf()));
    currDate.setDate(currDate.getDate() + 1);
  } while (currDate.toLocaleDateString() != firstOfNextMonth.toLocaleDateString()); // better way to compare date?

  return dates;
}

let nextId = 0;

const getNextAvailableId = () => {
  nextId++;
  return nextId;
};

Date.prototype.keyify = function () {
  return this.toISOString().split("T")[0];
};