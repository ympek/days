import express from "express";
import logger from "morgan";
import pg from "pg";

const port = 3000;

const app = express();

// for debugging purposes
function spinwait(seconds) {
  var waitTill = new Date(new Date().getTime() + seconds * 1000);
  while(waitTill > new Date()){}
}

// log every request
// P.S dev is a format, other formats can be seen on https://github.com/expressjs/morgan
app.use(logger('dev'));

// serve anything they request - dangerous but easy
app.use(express.static('.'));

// callback hell already 
app.get('/all', (req, res) => {
  const client = new pg.Client({
    user: 'days_user',
    host: 'localhost',
    database: 'days_development',
    password: 'dupa888',
    port: 5432,
  });

  client.connect();

  client.query('SELECT * FROM days', (db_err, db_res) => {
    console.log("query performed", db_err, db_res);

    res.json(db_res.rows);

    client.end()
  });

  // some delay, to test if client works fine : )
  // spinwait(2);
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("App is working");
    
    
  }
});

